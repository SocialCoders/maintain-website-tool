import subprocess
import json
import random
import os

urls = {
    "https://example.org": 2,
    "https://example.com": 2,
}

options = ["-j 16","-j 12", "-t 2"]

def test_check():
    args = ["poetry run python3 -m maintain_website_tool",
            "link",
            "check"]
    args.extend(random.sample(options, 2))
    args.append("-")

    result = json.loads(subprocess.run(args, input = list(urls)[0], encoding = "utf-8").stdout)

    assert len(result) == urls[list(urls)[0]]
    assert all([result[u]["ok"] for u in result])
